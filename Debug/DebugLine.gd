tool
extends ImmediateGeometry

var vertices
	
func set_vertices(new_vertices):
	vertices = new_vertices

func _process(delta):
	self.clear()
	self.begin(PrimitiveMesh.PRIMITIVE_LINES)
	self.set_color(Color(1,1,1))
	for vertex in vertices:
		self.add_vertex(vertex) 
	self.end()
