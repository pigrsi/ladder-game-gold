extends Node

const DebugLineResource = preload("res://Debug/DebugLine.tscn")
const DebugBollResource = preload("res://Debug/DebugBoll.tscn")

func draw_line(vertices):
	var line = DebugLineResource.instance()
	line.set_vertices(vertices)
	self.add_child(line)

func add_boll(translation):
	var boll = DebugBollResource.instance()
	boll.translation = translation
	self.add_child(boll)
