extends Camera

export(NodePath) var targetPath
var target
var viewing_angle = 0
var current_rotate_speed = 0

const UP = Vector3(0, 1, 0)
const OFFSET = Vector3(0, 5, 7.5)

func _ready():
	target = get_node(targetPath)

func _physics_process(delta):
	viewing_angle += current_rotate_speed * delta
	var destination = target.translation + OFFSET.rotated(UP, viewing_angle)
	
	translation = destination
	look_at(target.translation, UP)

func _unhandled_input(_event):
	current_rotate_speed = (
	Input.get_action_strength("camera_right") - Input.get_action_strength("camera_left")) * -7
