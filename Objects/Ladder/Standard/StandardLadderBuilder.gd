tool
extends "res://Objects/Ladder/LadderBuilder.gd"

func do_build():
	.do_build()
	
	var ladder = get_parent()
	ladder.get_node("TopClimbArea").height = ladder.length * 2
	ladder.get_node("BottomClimbArea").height = ladder.length * 2
