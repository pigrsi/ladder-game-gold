extends RigidBody

export var length: float = 1

var grabbed
var normal_collision_layer
var normal_collision_mask

func _ready():
	normal_collision_layer = collision_layer
	normal_collision_mask = collision_mask
	
func _physics_process(delta):
	$Blocker.global_transform.origin = transform.origin
	$Blocker.global_transform.basis = Basis(transform.basis)

func on_grabbed():
	$Blocker.on_grabbed()
	mode = RigidBody.MODE_KINEMATIC
	collision_layer = 0
	collision_mask = 0
	$MeshInstance.material_override = load("res://GFX/Materials/Ladder/LadderTrans.material")
	grabbed = true
	
func on_dropped():
	$Blocker.on_dropped()
	mode = RigidBody.MODE_RIGID
	collision_layer = normal_collision_layer
	collision_mask = normal_collision_mask
	$MeshInstance.material_override = null
	grabbed = false

func highlight():
	if grabbed: return
	$MeshInstance.material_override = load("res://GFX/Materials/Ladder/LadderGlow.material")
	
func dehighlight():
	if grabbed: return
	$MeshInstance.material_override = null
