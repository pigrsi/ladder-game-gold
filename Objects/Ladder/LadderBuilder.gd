tool
extends Node

func _ready():
	var ladder = get_parent()
	var blocker = ladder.get_node("Blocker/CollisionShape")
	blocker.shape = blocker.shape.duplicate()
	var main_collider = ladder.get_node("BoxCollision")
	main_collider.shape = main_collider.shape.duplicate()
	do_build()
	
func _process(delta):
	if not Engine.editor_hint: return
	do_build()
	
func do_build():
	var ladder = get_parent()
	var length = ladder.length
	var bottom = ladder.get_node("Rail/Bottom")
	var top = ladder.get_node("Rail/Top")
	
	ladder.get_node("MeshInstance").scale.z = length
	bottom.translation.z = length
	top.translation.z = -length
	
	var main_collider: CollisionShape = ladder.get_node("BoxCollision")
	main_collider.shape.extents.z = length
	
	var blocker_shape: CollisionShape = ladder.get_node("Blocker/CollisionShape")
	blocker_shape.shape.extents.z = length + 0.1
