extends RigidBody

var normal_collision_layer
var normal_collision_mask

func _ready():
	normal_collision_layer = collision_layer
	normal_collision_mask = collision_mask

func on_grabbed():
	collision_layer = 0
	collision_mask = 0
	
func on_dropped():
	collision_layer = normal_collision_layer
	collision_mask = normal_collision_mask
