extends Area

export var width: float = 1
export var height: float = 1
export var collision_distance = 0.05

func _on_ClimbArea_body_entered(body):
	if body.has_method("on_ClimbArea_entered"):
		body.on_ClimbArea_entered(self)

func _on_ClimbArea_body_exited(body):
	if body.has_method("on_ClimbArea_exited"):
		body.on_ClimbArea_exited(self)
