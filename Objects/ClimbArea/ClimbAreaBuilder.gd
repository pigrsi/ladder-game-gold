tool
extends Node

func _ready():
	var parent = get_parent()
	var collision_shape = parent.get_node("CollisionShape")
	collision_shape.shape = collision_shape.shape.duplicate()
	do_build()
	
func _process(delta):
	if not Engine.editor_hint: return
	do_build()
	
func do_build():
	var parent = get_parent()
	var width = parent.width
	var height = parent.height
	var collision_distance = parent.collision_distance
	
	var collision_shape = parent.get_node("CollisionShape")
	var mesh_instance = parent.get_node("MeshInstance")
	
	collision_shape.shape.extents.x = width/2
	collision_shape.shape.extents.y = height/2
	collision_shape.shape.extents.z = collision_distance
	
	mesh_instance.scale.x = width
	mesh_instance.scale.z = height
	mesh_instance.translation.z = -collision_distance
