extends "res://Characters/Dan/Scripts/DanScript.gd"

enum LiftMode {
	LONG, TALL_FORWARD, TALL_BACKWARD
}

var lift_mode = LiftMode.LONG
var ladder: RigidBody
var ladder_start_node: Spatial
var ladder_end_node: Spatial
var rail_offset: float
var rail_length: float
var height_offset
var rotation_offset

var slide_speed = 0
var lift_speed = 0
var flip_slide_directions = false

func is_holding_something():
	return ladder != null
	
func horizontal_setup():
	lift_mode = LiftMode.LONG
	height_offset = (ladder.translation.y - body.translation.y) * Vector3.UP
	
	var rail = ladder.get_node("Rail")
	flip_slide_directions = ladder.transform.basis.z.dot(body.transform.basis.z) < 0
	ladder_start_node = pdc(rail.get_child(0), rail.get_child(1))
	ladder_end_node = pdc(rail.get_child(1), rail.get_child(0))
	
	rotation_offset = Vector3(0, pdc(0, PI), ladder.rotation.z)	
	
	rail_length = (ladder_start_node.transform.origin - ladder_end_node.transform.origin).length()
	var offsetVector: Vector3 = ladder_start_node.global_transform.origin - body.translation
	offsetVector.y = 0
	rail_offset = offsetVector.length()

	
func vertical_setup():
	lift_mode = LiftMode.TALL_BACKWARD
	
	var rail = ladder.get_node("Rail")
	var dist_0 = (rail.get_child(0).global_transform.origin - body.translation).length_squared()
	var dist_1 = (rail.get_child(1).global_transform.origin - body.translation).length_squared()
	
	flip_slide_directions = dist_0 > dist_1
	ladder_start_node = pdc(rail.get_child(0), rail.get_child(1))
	ladder_end_node = pdc(rail.get_child(1), rail.get_child(0))
		
	height_offset = (ladder_start_node.global_transform.origin.y - body.translation.y) * Vector3.UP
	rail_length = (ladder_start_node.transform.origin - ladder_end_node.transform.origin).length()
	
	rotation_offset = Vector3(PI/2, pdc(0, PI), 0)	
	
	rail_offset = 0
	
	
func physics_tick(delta):
	if not ladder: return
	
	adjust_LONG_offset(delta) if lift_mode == LiftMode.LONG else adjust_TALL_offset(delta)
	
	# Rotation
	var min_rotation_offset = get_min_rotation_offset()
	var max_rotation_offset = get_max_rotation_offset()
	
	rotation_offset.x += lift_speed * delta
	
	if rotation_offset.x < min_rotation_offset or rotation_offset.x > max_rotation_offset:
		switch_lift_mode(LiftMode.LONG)
	
	rotation_offset.x = clamp(rotation_offset.x, min_rotation_offset, max_rotation_offset)
	
	ladder.rotation = body.rotation + pda(rotation_offset)
	
	# Position
	var ladder_start = ladder_start_node.global_transform.origin
	var ladder_end = ladder_end_node.global_transform.origin
	
	if lift_mode == LiftMode.LONG:
		# Ladder should slide along the player
		var final_offset = height_offset
		final_offset += (rail_length / 2) * (ladder.translation - ladder_start).normalized()
		final_offset += rail_offset * body.transform.basis.z
		ladder.translation = body.translation + final_offset
	else:
		# Ladder should pivot about endpoint
		if rail_offset > 0:
			ladder.translation += body.transform.origin - ladder_end + height_offset
		else:
			ladder.translation += body.transform.origin - ladder_start + height_offset
			
	
func adjust_LONG_offset(delta):
	if slide_speed == 0: return
	
	rail_offset += slide_speed * 4 * delta
		
	if rail_offset < 0 or rail_offset > rail_length:
		switch_lift_mode(LiftMode.TALL_FORWARD if slide_speed > 0 else LiftMode.TALL_BACKWARD)
		
	rail_offset = clamp(rail_offset, 0, rail_length)


func adjust_TALL_offset(delta):
	lift_speed = 0
	if slide_speed == 0: return
	
	lift_speed = slide_speed * -3
	
	
func switch_lift_mode(new_mode):
	lift_speed = 0
	
	var swap_direction = ldc(rotation_offset.x >= get_max_rotation_offset(), rotation_offset.x <= get_min_rotation_offset())
	if new_mode == LiftMode.LONG and swap_direction:
		# Reset for the other direction
		flip_slide_directions = not flip_slide_directions
		var temp = ladder_start_node
		ladder_start_node = ladder_end_node
		ladder_end_node = temp
		rail_offset = ldc(rail_length, 0)
		rotation_offset.x = 0
		rotation_offset.y = pdc(0, PI)
		rotation_offset.z += PI
		
	lift_mode = new_mode
	
	
func get_min_rotation_offset():
	return ldc( 0, -PI )
	
func get_max_rotation_offset():
	return ldc( PI, 0 )
	
func handle_input(event):
	if not ladder: return
	if Input.get_action_strength("ladder_slide_forward") != 0:
		slide_speed = 1
	elif Input.get_action_strength("ladder_slide_backward") != 0:
		slide_speed = -1
	else:
		slide_speed = 0
		
	if event.is_action_pressed("grab"):
		ladder.on_dropped()
		emit_signal("change_state", behaviour_manager.State.NORMAL)
		ladder = null
		
	
func pda(value):
	# Value adjusted for player direction
	return -value if flip_slide_directions else value
	
func pdc(reg_choice, flipped_choice):
	# Choice depending on player direction
	return flipped_choice if flip_slide_directions else reg_choice
	
func ldc(backward_choice, forward_choice):
	# Choice depending on lifting direction
	return forward_choice if lift_mode == LiftMode.TALL_FORWARD else backward_choice
	
func set_target(target):
	ladder = target

	if abs(Vector3.UP.dot(ladder.transform.basis.z)) < 0.5:
		horizontal_setup()
	else: vertical_setup()
	
	height_offset = Vector3.UP/2
