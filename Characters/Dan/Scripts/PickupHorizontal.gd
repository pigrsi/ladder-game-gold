extends Node

var target_path: Curve3D
var target_progress

var body_rotation_start: Quat
var body_rotation_dest: Quat

func setup(body, target):
	var tween = $Tween
	
	# Figure out which direction to look at
	var rail = target.get_node("Rail")
	var railStart = rail.get_child(0).global_transform.origin
	var railEnd = rail.get_child(1).global_transform.origin
	
	# Figure out grabber body rotation
	var mult = 1
	if (railStart - body.translation).length() < (railEnd - body.translation).length():
		mult = -1
	
	body_rotation_start = Quat(body.transform.basis)
	body_rotation_dest = Quat(body.transform.looking_at(
		body.translation + target.transform.basis.z * mult, Vector3.UP).basis)
	
	# Create path
	var start = target.translation
	var dest = get_target_destination(body, target)
	target_path = Curve3D.new()
	target_path.add_point(start, Vector3.ZERO, Vector3.UP * 2)
	target_path.add_point(dest)
	tween.interpolate_property(self, "target_progress", 0, 1, 0.5, Tween.TRANS_LINEAR)
	
	tween.start()
	
func tick(body, target):
	var hold_position = target_path.interpolate_baked(target_progress * target_path.get_baked_length())
	target.translation = hold_position
	body.transform.basis = Basis(body_rotation_start.slerp(body_rotation_dest, target_progress))
	body.rotation.x = 0
	body.rotation.z = 0
	
	return target_progress >= 1

func get_target_destination(grabber: PhysicsBody, target: PhysicsBody):
	var rail = target.get_node("Rail")
	var railStart = rail.get_child(0).global_transform.origin
	
	var destination = grabber.translation + (target.translation - railStart)
	
	destination -= find_pickup_offset_on_rail(grabber, target)
	destination.y = grabber.translation.y + 0.5
	
	return destination
	
func find_pickup_offset_on_rail(grabber: PhysicsBody, target: PhysicsBody):
	var rail = target.get_node("Rail")
	var start = rail.get_child(0).global_transform.origin
	var end = rail.get_child(1).global_transform.origin
	var startToEnd = end - start

	var alongRailPlane = Plane(start, start + Vector3.ONE * target.transform.basis.y, end)
	var acrossRailPlane = Plane(start, start + Vector3.ONE * target.transform.basis.x, end)
	var grabberProjectedPosition = acrossRailPlane.project(alongRailPlane.project(grabber.translation))
	
	var startToGrabber = grabberProjectedPosition - start
	
	# Grab at the very end
	if (end - grabberProjectedPosition).length() > (end - start).length():
		return Vector3()
	elif (start - grabberProjectedPosition).length() > (end - start).length():
		return startToEnd
	
	# Grab somewhere in the middle
	return startToGrabber
