extends "res://Characters/Dan/Scripts/DanScript.gd"

var rotate_speed := 0.0


func physics_tick(delta):
	var movementVector: Vector2 = get_input()
	var aim = camera.get_global_transform().basis
	
	var moveVelocity = Vector3()
	moveVelocity += aim.x * movementVector.x * 7
	moveVelocity += -aim.z * movementVector.y * 7
	moveVelocity.y = body.velocity.y	
	body.velocity = moveVelocity
	
	# Rotation
	var walk_movement = Vector3(body.velocity.x, 0, body.velocity.z)
	if walk_movement.length_squared() > 0:
		var targetBasis = body.transform.looking_at(body.translation + walk_movement, Vector3.UP)
		body.transform.basis = Basis(Quat(body.transform.basis).slerp(targetBasis.basis, 0.2))
	body.rotate_y(rotate_speed * delta)
		
	
func get_input() -> Vector2:
	var inputVector = Vector2()
	
	inputVector.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	inputVector.y = Input.get_action_strength("move_up") - Input.get_action_strength("move_down")
	inputVector = inputVector.normalized()
	
	return inputVector
