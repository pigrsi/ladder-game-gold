extends "res://Characters/Dan/Scripts/DanScript.gd"

const GRAVITY = 9.8

func physics_tick(delta):
	if body.is_on_floor() and not body.just_jumped:
		body.velocity.y = 0
	
	body.velocity.y -= GRAVITY * delta	
	
