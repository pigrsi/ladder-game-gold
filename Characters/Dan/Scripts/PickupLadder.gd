extends "res://Characters/Dan/Scripts/DanScript.gd"

var grabbables = {}
var target: PhysicsBody
var pickupNode

func attempt_grab():
	if grabbables.empty(): return
	
	target = grabbables.get(grabbables.keys()[0])
	
	target.on_grabbed()
	
	var dotted = Vector3.UP.dot(target.transform.basis.z)
	pickupNode = $Horizontal if abs(dotted) < 0.3 else $Vertical
	
	pickupNode.setup(body, target)
	
	emit_signal("change_state", behaviour_manager.State.PICKING_UP)
	
func tick(delta):
	if not target: return
	
	if pickupNode.tick(body, target):
		body.currently_held_object = target
		emit_signal("change_state", behaviour_manager.State.HOLDING)
		target = null

func handle_input(event):
	if event.is_action_pressed("grab"):
		attempt_grab()

# --- Signals --- #
func _on_GrabArea_body_entered(body):
	grabbables[body.name] = body
	if body.has_method("highlight"): body.highlight()

func _on_GrabArea_body_exited(body):
	grabbables.erase(body.name)
	if body.has_method("dehighlight"): body.dehighlight()
