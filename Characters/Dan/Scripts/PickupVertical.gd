extends Node

var rotation_adjustment = { "vector": null, "angle": 0 }
var target_path: Curve3D
var target_progress

var target_rotation_start: Quat
var target_rotation_dest: Quat

func setup(body: PhysicsBody, target: PhysicsBody):
	# Create path
	var start = target.translation
	var dest = body.translation + (Vector3.UP * 3.5) + (body.transform.basis.z * -0.01)
	target_path = Curve3D.new()
	target_path.add_point(start, start, Vector3.UP * 3)
	target_path.add_point(dest, Vector3.UP * 3, dest)
	
	# Tween position and rotation
	var rail = target.get_node("Rail")
	var railBottom = rail.get_child(0).global_transform.origin
	var railTop = rail.get_child(1).global_transform.origin

	var aim_multiplier = -1 if railBottom.y > railTop.y else 1
	target_rotation_start = Quat(target.transform.basis)
	var dest_transform = target.transform.looking_at(target.translation + Vector3.UP * (start - dest).length_squared() * aim_multiplier, body.transform.basis.z)
	target_rotation_dest = Quat(dest_transform.basis)
	
	var tween = $Tween
	tween.interpolate_property(self, "target_progress", 0, 1, 1, Tween.TRANS_LINEAR)
	tween.start()
	
func tick(body, target):
	var hold_position = target_path.interpolate_baked(target_progress * target_path.get_baked_length())
	target.translation = hold_position
	target.transform.basis = Basis(target_rotation_start.slerp(target_rotation_dest, target_progress))
	
	return target_progress >= 1
