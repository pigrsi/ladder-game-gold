extends "res://Characters/Dan/Scripts/DanScript.gd"

const UP = Vector3(0, 1, 0)

func physics_tick(_delta):
	var snap = Vector3.ZERO if body.just_jumped else Vector3.DOWN
	if body.velocity == Vector3.ZERO:
		body.move_and_slide(body.velocity, Vector3.UP, true, 4, 0.785398, false)
	else:
		body.move_and_slide_with_snap(body.velocity, snap, Vector3.UP, true, 4, 0.785398, false)
		body.just_jumped = false
