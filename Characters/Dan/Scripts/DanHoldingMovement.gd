extends "res://Characters/Dan/Scripts/DanScript.gd"

var rotate_speed := 0.0

func physics_tick(delta):
	var movementVector: Vector2 = get_input()
	var aim = camera.get_global_transform().basis
	
	var moveVelocity = Vector3()
	moveVelocity += aim.x * movementVector.x * 7
	moveVelocity += -aim.z * movementVector.y * 7
	moveVelocity.y = body.velocity.y	
	body.velocity = moveVelocity
	
	# Rotation
	body.rotate_y(rotate_speed * delta)
		
func get_input() -> Vector2:
	var inputVector = Vector2()
	
	inputVector.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	inputVector.y = Input.get_action_strength("move_up") - Input.get_action_strength("move_down")
	inputVector = inputVector.normalized()
	
	return inputVector
	
func handle_input(e):
	if e.is_action("rotate_left"):
		rotate_speed = 4 if e.is_pressed() else 0
	elif e.is_action("rotate_right"):
		rotate_speed = -4 if e.is_pressed() else 0
