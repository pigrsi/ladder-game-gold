extends Spatial

enum State {
	NORMAL,
	PICKING_UP,
	HOLDING,
	CLIMBING
}

var state = State.NORMAL
var body

func _ready():
	body = get_parent()
	var camera = get_tree().get_root().find_node("Camera", true, false)
	for child in get_children():
		child.body = body
		child.behaviour_manager = self
		child.camera = camera
		child.connect("change_state", self, "_on_state_changed")


func _unhandled_input(e):
	match(state):
		State.NORMAL:
			$Pickup.handle_input(e)
		State.HOLDING:
			$HoldingMovement.handle_input(e)
			$Holding.handle_input(e)
		
		
func _process(delta):
	match(state):
		State.NORMAL:
			$Jumping.tick(delta)
		State.PICKING_UP:
			$Pickup.tick(delta)
		State.HOLDING:
			$Jumping.tick(delta)
		State.CLIMBING:
			$Climbing.tick(delta)
		
		
func _physics_process(delta):
	match(state):
		State.NORMAL:
			$NormalMovement.physics_tick(delta)
			$Falling.physics_tick(delta)
			$Collision.physics_tick(delta)
		State.HOLDING:
			$HoldingMovement.physics_tick(delta)
			$Collision.physics_tick(delta)			
			$Holding.physics_tick(delta)
			$Falling.physics_tick(delta)
	
	
func _on_state_changed(new_state):
	print("Player state changed from " + State.keys()[state] + " to " + State.keys()[new_state] )
	
	# Exiting old state
	match(state):
		State.CLIMBING:
			body.velocity = Vector3.ZERO
		
	state = new_state
	
	# Entering new state
	match(state):
		State.HOLDING:
			$Holding.set_target(body.currently_held_object)
	

func on_ClimbArea_entered(area):
	if state == State.NORMAL:
		$Climbing.on_ClimbArea_entered(area)
	
func on_ClimbArea_exited(area):
	pass
