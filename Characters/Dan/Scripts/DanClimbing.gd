extends "res://Characters/Dan/Scripts/DanScript.gd"

var area: Area
var climb_offset: Vector3
var down_is_up

func is_climbing_something():
	return area != null


func on_ClimbArea_entered(area):
	self.area = area
	
	var AREA_RIGHT = area.global_transform.basis.x
	var AREA_UP = area.global_transform.basis.y
	
	var area_plane = Plane(area.global_transform.origin,
			area.global_transform.origin + AREA_RIGHT,
			area.global_transform.origin + AREA_UP
	)
	
	down_is_up = Vector3.UP.dot(AREA_UP) < 0
	
	var area_bottom_left = (area.global_transform.origin
			- area.width/2 * AREA_RIGHT
			- area.height/2 * AREA_UP)
			
	var projected_translation = area_plane.project(body.translation)
	
	climb_offset = projected_translation - area_bottom_left
	climb_offset = Vector3(
		climb_offset.project(AREA_RIGHT).length() - area.width/2,
		climb_offset.project(AREA_UP).length() - area.height/2,
		0
	)
	
	emit_signal("change_state", behaviour_manager.State.CLIMBING)
	
	
func tick(delta):
	if not area: return
	
	climb_offset += get_input() / 12
	
	var RIGHT = area.global_transform.basis.x
	var UP = area.global_transform.basis.y

	body.translation = (area.global_transform.origin + 
			(climb_offset.x * RIGHT) + 
			(climb_offset.y * UP))
	
	body.rotation = area.global_transform.basis.get_euler()
	if abs(climb_offset.x) > area.width/2 or abs(climb_offset.y) > area.height/2:
		emit_signal("change_state", behaviour_manager.State.NORMAL)
	
func get_input() -> Vector3:
	var inputVector = Vector3()
	
	inputVector.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	inputVector.y = Input.get_action_strength("move_up") - Input.get_action_strength("move_down")
	if down_is_up: inputVector *= -1
	inputVector = inputVector.normalized()
	
	return inputVector
