extends KinematicBody

var velocity = Vector3()
var just_jumped = false
var currently_held_object

func on_ClimbArea_entered(area):
	$Behaviour.on_ClimbArea_entered(area)
	
func on_ClimbArea_exited(area):
	$Behaviour.on_ClimbArea_exited(area)
